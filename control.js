const ipc = require('./lib/u_ipc');
const timer = require('./lib/timer');
const util = require('./lib/util');
const http = require('http');

var cb0 = function(src) {
    console.log("[cb] -------------------> cb0 from:"+src);
}

ipc.init(3000,cb0);
timer.init();

var tmCallBack = function() {
    console.log("[cb] -------------------> tmCallBack");
    console.log("[tmCallBack] uuid:"+util.getUUID())
}

http.createServer(function (req, res) {
    console.log("start server");
    console.log(util.getTimeStr('YYYY-MM-DD HH:mm:ss'));
    var data = {
        a:11,
        b:22,
        c:[1,2,3]
    }
    ipc.send(util.getUUID(), 4000, data);
    timer.setTimer(5,tmCallBack);
}).listen(3000);