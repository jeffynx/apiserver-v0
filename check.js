var myMethod = function () {
    console.log(this);
  };
  
  var obj1 = {
    a: 2,
    myMethod: myMethod
  };
  
  var obj2 = {
    a: 3,
    myMethod: myMethod
  };
  
  obj1.myMethod(); // 2
  obj2.myMethod(); // 3
  
  obj1.myMethod.call( obj2 ); // ?????, 직접 해보세요.
  obj2.myMethod.call( obj1 ); // ?????