const ipc = require('./lib/u_ipc');
const util = require('./lib/util');
const http = require('http');

var cb1 = function(src, rcvData) {
    console.log("[cb] -------------------> cb1 from:"+src);
    console.log(rcvData);
    ipc.send(util.getUUID(), src, 'hi~~~ cb');
}

ipc.init(4000,cb1);