'use strict'

let timerList = [];
let timerObj = {};
let timerLoop = function() {
    console.log("[timerLoop] current length:" + timerList.length);

    timerList.forEach(function(d){
        console.log(`d --> val(${d.val})`);
        d.ucb();
        timerList.pop(d);
    })
}

module.exports.init = function() {
    console.log('[timer.js] init');
    setInterval(timerLoop, 1000);
}

module.exports.setTimer = function(v, cb) {
    var obj = {};
    obj.val = v;
    obj.ucb = cb;
    timerList.push(obj);
}