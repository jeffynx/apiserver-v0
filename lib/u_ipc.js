'use strict';

var dgram = require('dgram');
var comm = dgram.createSocket('udp4');
let userCb;
let ipcData = {
    hdr:null,  /* cid: context id */
    body:null /* json data */
}

module.exports.init = function(port, cb) {

    userCb = cb;
        
    comm.on('error', (err) => {
        console.log(`comm error:\n${err.stack}`);
        comm.close();
    });
    
    comm.on('message', (msg, rinfo) => {
        console.log('[recv] -------------------------------------------');
        var data = JSON.parse(msg);
        console.log(data.hdr);
        console.log(data.body);
        
        userCb(rinfo.port, data);
        console.log(`comm got: ${msg} from ${rinfo.address}:${rinfo.port}`);
    });
    
    comm.on('listening', () => {
        const address = comm.address();
        console.log(`comm listening ${address.address}:${address.port}`);
    });
    
    comm.bind(port);
}

/* dest: null(to router), not null (send to), all (multi case) */
module.exports.send = function(id, dest, msg) {

    console.log("[u_ipc] send");

    var sendData = ipcData;
    sendData.hdr = id;
    sendData.body = msg;

    // var sendStr = new Buffer.from(JSON.stringify(sendData));
    var sendStr = JSON.stringify(sendData);

    console.log(sendStr);

    comm.send(sendStr, 0, sendStr.length, dest, '127.0.0.1', function(err, bytes) {
        if (err) {
            console.log(err);
            throw err;
        }
        console.log('UDP message sent!!!! ' + '127.0.0.1' + ':' + dest);
        // comm.close();
    });
}

module.exports.close = function() {
    comm.close();
}