var uuid4 = require('uuid4');
var moment = require('moment');

function getRandomClientId() {
    return Math.random()
        .toString(36)
        .substring(2)
        .toUpperCase();
}

module.exports.getUUID = function() {
    const tokens = uuid4().split('-')
    return tokens[2] + tokens[1] + tokens[0] + tokens[3] + tokens[4]
}

/*
    ex)
    format: 'YYYY-MM-DD HH:mm:ss'
*/
module.exports.getTimeStr = function(f) {
    return moment().format(f);
}